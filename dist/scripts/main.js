"use strict";

$(document).ready(function () {
  $('.phone').inputmask('+7(999)999-99-99');
  $('.k-home-reviews__cards').slick({
    dots: true,
    arrows: false,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    centerMode: true,
    autoplay: true,
    autoplaySpeed: 4000,
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    }]
  });
  $('.k-header__toggle').on('click', function (e) {
    e.preventDefault();
    $('.k-mob-nav').slideToggle('fast');
  });
  $('.k-mob-nav__close').on('click', function (e) {
    e.preventDefault();
    $('.k-mob-nav').slideToggle('fast');
  });
  $('.k-header__search').on('click', function (e) {
    e.preventDefault();
    document.getElementById("myOverlay").style.display = "block";
  });
  $('.closebtn').on('click', function (e) {
    e.preventDefault();
    document.getElementById("myOverlay").style.display = "none";
  });
});
//# sourceMappingURL=main.js.map
